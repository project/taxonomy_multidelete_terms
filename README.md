# CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - How To Use

## Introduction

This module makes it possible to delete multiple term at one time.
You need to select terms and click on delete button.
All selected terms will be delete. You can also delete all terms at one time.

## Requirements

This module requires the following modules:
 - Taxonomy

## Installation

 - Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
   for further information.

## How To Use

 - Go to Structure - Taxonomy - list terms (if you have added vocabulary).
 - Select check-box (in front of the list item).
 - Click on delete button.
